package main

import "sort"

/*
Реализовать бинарный поиск встроенными методами языка.
*/

func task17() {
	arr := []int{1, 2, 3, 4, 5}
	target := 2

	index := sort.Search(len(arr), func(i int) bool { return arr[i] >= target })
	println(index)
}

func task17_2(arr []int, target int) int {
	// Ставим верхнюю и нижнию границу
	low, high := 0, len(arr)-1

	for low <= high {
		// Находим середину
		mid := low + (high-low)/2

		// Проверяем подходил ли число
		if arr[mid] == target {
			return mid
		}

		if arr[mid] < target {
			// Если искомое число больше, то нужно уйти правее
			low = mid + 1
		} else {
			// Если искомое число меньше, то нужно уйти левее
			high = mid - 1
		}
	}

	// Если не нашли число
	return -1
}
