package main

import (
	"fmt"
	"sort"
)

/*
Реализовать быструю сортировку массива (quicksort) встроенными методами
языка.
*/

func task16() {
	arr := []int{5, 4, 3, 2, 1}

	sort.Ints(arr)

	fmt.Println(arr)
}

func task16_2() {
	arr := []int{5, 4, 3, 2, 1}

	quickSort(arr, 0, len(arr)-1)

	fmt.Println(arr)
}

func quickSort(arr []int, low int, high int) {
	if low < high {
		// Находим индекс опорного элемента для разделения массива
		pivot := partition(arr, low, high)

		// Рекурсивно сортируем подмассивы до и после опорного элемента
		quickSort(arr, low, pivot-1)
		quickSort(arr, pivot+1, high)
	}
}

func partition(arr []int, low int, high int) int {
	// Выбираем опорный элемент как последний элемент массива
	pivot := arr[high]
	// Инициализируем индекс меньшего элемента
	index := (low - 1)

	// Проходим по элементам массива от low до high-1
	for i := low; i <= high-1; i++ {
		// Если текущий элемент меньше опорного
		if arr[i] < pivot {
			// Увеличиваем индекс меньшего элемента и меняем элементы местами
			index++
			arr[index], arr[i] = arr[i], arr[index]
		}
	}
	// Помещаем опорный элемент в правильную позицию
	arr[index+1], arr[high] = arr[high], arr[index+1]
	// Возвращаем индекс опорного элемента
	return (index + 1)
}
